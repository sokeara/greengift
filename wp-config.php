<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'greengift');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-ytY~=*[etP|x@^-yWt63<;fNd_meW8mUtAa%q$O>#at+?+uY+e|&C<-]ajc@@lN');
define('SECURE_AUTH_KEY',  'Zn]&KwM,A^-.IsY`.,o]RI.U6xwkEiVa9wN[j;I.!Rf;~Go_*vax/5D[8QC%R]%O');
define('LOGGED_IN_KEY',    '/*&*.d|OjHPCD9P)cR6GXHB++;&U2-k`c!;f+yvA<[4`5}[pfAWDA{MGW}n*D*Yv');
define('NONCE_KEY',        'SkL2C/)SutM0wc-[vIG~:8tI~t~h..^l^gH|lb?K _L?}]-e$G&%%0r`&#+4w}Kz');
define('AUTH_SALT',        'O0e%z@+DcrnoC~!3 P^#>HQ(P{+/m=a3@*4_TT2c*UH.A*PhH7q8$^-$/TJdXb&/');
define('SECURE_AUTH_SALT', '6=FQ;8+{t&+Pri:MW2f^l4p:Xta>dTrS|-8SXXp_6rM{rEI[DHye!{>mS45FP&2_');
define('LOGGED_IN_SALT',   'h<R6ranG0>@2l`4SOpm>Vd>%kl$vihrL?q(Q|!Rc0 xNlu$p5%~j%WGT1+/M+Z*s');
define('NONCE_SALT',       'ipF>KpqgI:+=&o(i]S3c$p&}g+oQBR)V`jlM%1L* [XU50|$Y%xj60]1<K[Kb)`%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');